
Twitpic Filter module
http://drupal.org/project/twitpic_filter
by Ben Buckman (thebuckst0p)
email thebuckst0p at gmail

== Summary ==
This module allows Twitpic (http://twitpic.com) images to be displayed directly on a Drupal site, allowing Twitter and Twitpic to be used as a mobile engine for a photo-rich blog.

== Background ==
Twitpic allows images to be posted to Twitter, appearing as URLs in a Twitter feed. Pulling Twitter feeds into Drupal is a great way to get easy mobile micro-blogging, and many mobile Twitter apps include photo upload capability.

The problem is that the feed only shows URLs, not the images. Twitpic offers an API, but it only allows thumbnails to be viewed in a browser; full images have to be download.

This module uses an Input Filter to intercept Twitpic URLs, download their originals to the Drupal filesystem, and display the images instead of the URLs to the end user.

The module also integrates with ImageCache, if it's installed, to format the final image with a 'preset'.

== Instructions ==
TO ENABLE THE MODULE, activate the 'Convert Twitpic URLs to Images' filter in your Input Formats,
at admin/settings/filters on your Drupal backend.

IMPORTANT: The 'Filtered HTML' filter can conflict with Twitpic Filter if it runs AFTER Twitpic Filter (because it removes the image tag added for the twitpic). If a format is set up in the wrong order, the user should be notified (to change the order via the Rearrange tab), but keep this in mind when setting up.

== Status ==
Still in development, stability not guaranteed!

== Contributions ==
Any and all contributions/suggestions/improvements are welcome! email thebuckst0p at gmail.
