<?php

// pattern testing for twitpic filter
// (only use for testing/debugging)


$twitpic_pattern = '#(?<![\'".a-zA-Z0-9/])(http://twitpic.com/([a-zA-Z0-9]{5}))(?![\'".a-zA-Z0-9/])#im';
//$twitpic_pattern = '#[^\'\"=/.]?http://twitpic.com/([a-zA-Z0-9]{5})[^\'\"=/.]?#im';
//$twitpic_pattern = '#[^\'\"=/]http://twitpic.com/([a-zA-Z0-9]{5})[^\'\"=/]#im';		// worked in tester
//$twitpic_pattern = '|[^\'\"]http://twitpic.com/([a-z0-9]{5})[^"]|i';
//$twitpic_pattern = '|[^\'\"]http://twitpic.com/([a-z0-9]{5})[^/][^\'\"]|';
		// tested with http://gskinner.com/RegExr/

$tests = array(
	'http://twitpic.com/show/full/81abhzz',
	'http://twitpic.com/12345 Heading',
	' http://twitpic.com/12345',
	'<img src="http://twitpic.com/12345">',
	' src="http://twitpic.com/12345"',
	' src=\'http://twitpic.com/12345\' Heading',
	'http://twitpic.com/12345.jpg',
	'<z href="http://twitpic.com/12345">',
	"<z href='http://twitpic.com/12345'>",
	'http://twitpic.com/12345',
	'    http://twitpic.com/12345    ',
	'http://twitpic.com/12345',	
	'http://twitpic.com/12345/',	
	'http://twitpic.com/12345 http://twitpic.com/12345a',
	'    http://twitpic.com/12345a    ',	
	'    http://twitpic.com/12345a6    ',	
	);


foreach($tests as $test) {
	echo '<b>' . str_replace(' ', '_', htmlentities($test) ) . '</b> ';

	if (preg_match($twitpic_pattern, $test, $matches)) {
		echo '<pre>'. str_replace(' ', '&nbsp;', htmlentities(print_r($matches,true))) .'</pre>';
		
		$new_str = preg_replace($twitpic_pattern, '<img src="x.jpg">\2</img>', $test);
		echo '<pre>'. str_replace(' ', '&nbsp;', htmlentities($new_str) .'</pre>');
		
	}
	
	echo '<hr/>';	
}

