
Twitpic Filter Changelog

Twitpic Filter 6.x-1.0-dev1
----------------------
11/10/09: Initial commit of module


Twitpic Filter 6.x-1.0-beta1
----------------------
2/22/10:
- updated twitpic regex pattern to handle >5 characters
- refactored twitpic_filter to tokenize URLs, avoid conflict with other filters
- delegated hook_filter settings op to its own function
- added user warning for badly ordered filters
- updated README.txt
- updated intro docblock
- fixed imagecache theming



[reference]
Use this syntax for commit messages:
[issue category] #[issue number] by [comma-separated usernames]: [Short summary of the change].
